class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates_presence_of :title#title
  validates_presence_of :body#body
end
